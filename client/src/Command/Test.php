<?php


namespace App\Command;

use Grpc\ChannelCredentials;
use Proto\Order\OrderClient;
use Proto\Order\OrderRequest;
use Proto\Order\OrderResponse;
use Proto\Product\Product;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Test extends Command
{
    protected static $defaultName = 'app:client';

    protected function configure()
    {
        $this->setDescription('Test client for gPRC');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = new OrderClient('phpserver:9001', [
            'credentials' => ChannelCredentials::createInsecure(),
        ]);

        $request = new OrderRequest();
        $request->setId(1);

        $call = $client->GetOrder($request);
        /** @var $orderResponse OrderResponse */
        list($orderResponse, $status) = $call->wait();
        /** @var Product $product */
        foreach ($orderResponse->getProducts() as $product) {
            $output->writeln($product->getName());
        }

        $output->writeln($orderResponse->serializeToJsonString());
    }
}
