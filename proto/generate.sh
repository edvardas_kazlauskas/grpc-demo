#!/usr/bin/env bash

./bin/protoc --proto_path=./protos   --php_out=./../client/generated   --grpc_out=./../client/generated --plugin=protoc-gen-grpc=./../grpc/bins/opt/grpc_php_plugin ./protos/*.proto
./bin/protoc --proto_path=./protos   --php_out=./../phpserver/src   --php-grpc_out=./../phpserver/src --plugin=protoc-gen-php-grpc=./../phpserver/bin/protoc-gen-php-grpc ./protos/*.proto
