<?php
/**
 * Sample GRPC PHP server.
 */

use Spiral\Goridge;
use Spiral\RoadRunner;
use Spiral\GRPC\Server;
use Proto\Order;

ini_set('display_errors', 'stderr');
require "vendor/autoload.php";

$server = new Server();
$server->registerService(Order\OrderInterface::class, new OrderService());
$w = new RoadRunner\Worker(new Goridge\StreamRelay(STDIN, STDOUT));
$server->serve($w);
