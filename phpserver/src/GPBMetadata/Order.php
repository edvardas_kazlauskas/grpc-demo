<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: order.proto

namespace GPBMetadata;

class Order
{
    public static $is_initialized = false;

    public static function initOnce() {
        $pool = \Google\Protobuf\Internal\DescriptorPool::getGeneratedPool();

        if (static::$is_initialized == true) {
          return;
        }
        \GPBMetadata\Customer::initOnce();
        \GPBMetadata\Product::initOnce();
        $pool->internalAddGeneratedFile(hex2bin(
            "0a9f020a0b6f726465722e70726f746f120b70726f746f2e4f726465721a" .
            "0d70726f647563742e70726f746f221a0a0c4f7264657252657175657374" .
            "120a0a0269641801200128032281010a0d4f72646572526573706f6e7365" .
            "120a0a026964180120012803120e0a06616d6f756e741802200128011228" .
            "0a0870726f647563747318032003280b32162e70726f746f2e50726f6475" .
            "63742e50726f64756374122a0a08637573746f6d657218042001280b3218" .
            "2e70726f746f2e437573746f6d65722e437573746f6d6572324c0a054f72" .
            "64657212430a084765744f7264657212192e70726f746f2e4f726465722e" .
            "4f72646572526571756573741a1a2e70726f746f2e4f726465722e4f7264" .
            "6572526573706f6e73652200620670726f746f33"
        ), true);

        static::$is_initialized = true;
    }
}

