<?php
/**
 * Sample GRPC PHP server.
 */

use Proto\Order;
use Proto\Order\Person;
use Proto\Customer\Customer;
use Proto\Product\Product;
use Spiral\GRPC\ContextInterface;

class OrderService implements Order\OrderInterface
{
    /**
     * @param ContextInterface   $ctx
     * @param Order\OrderRequest $request
     *
     * @return Order\OrderResponse
     */
    public function GetOrder(ContextInterface $ctx, Order\OrderRequest $request): Order\OrderResponse
    {
        $orderId = $request->getId();

        $total = $orderId * 42.99;

        $customer = new Customer();
        $customer->setId(1);
        $customer->setName('Edvardas');

        $product = new Product();
        $product->setId(1);
        $product->setName('Jeans');
        $product->setAmount($total);

        $order = new Order\OrderResponse();

        $order->setId(1);
        $order->setAmount($total);
        $order->setProducts([$product]);
        $order->setCustomer($customer);

        $person = new Person();
        $person->setId(1);
        $person->setName('Edvardas');
        $person->setEmail('edvardas.k@estina.lt');

        return $order;
}
