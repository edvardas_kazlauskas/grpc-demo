const PROTO_PATH = __dirname + '/../proto/protos/order.proto';

const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });

const order_proto = grpc.loadPackageDefinition(packageDefinition);

/**
 * @param call
 * @param callback
 * @constructor
 */
function GetOrder(call, callback) {
    let id = call.request.id;
    callback(null, {
        id: id,
        amount: id * 123.4,
        customer: {
            id: 1,
            name: 'Edvardas'
        },
        products: []
    });
}

function GetProducts(call) {
    let id = call.request.id;

    for (let i = 0; i < 300000; i++) {
        call.write({
            id: i * id,
            name: 'Product ' + i,
            amount: Math.round(Math.random() * 100)
        });
    }

    call.end();

}

/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */
function main() {
    const server = new grpc.Server();
    server.addService(order_proto.proto.Order.Order.service, {
        GetOrder: GetOrder,
        GetProducts: GetProducts
    });
    server.bind('0.0.0.0:3000', grpc.ServerCredentials.createInsecure());
    server.start();
}

main();
